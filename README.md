# Project Design and Management (PRJM3000)
Department of Computing

Semester 1, 2016

## Project Requirements

* Users should be able to access the platform online
* Users should be able to register and login to the platform
* Users should be able to explain an idea, innovation or solution (henceforth, referred to as submission) using text, images, videos, etc.
* Users should be rewarded for posting submissions
* Users should be able to ‘upvote’ or ‘downvote’ submissions
* Voting should not be a static increment or decrement. Users should be able to select a vote ‘weight’ to give a good idea more votes (or vice versa)
* Users should be rewarded for suggesting improvements to a submission
* Users should be able to ‘upvote’ or ‘downvote’ improvements to a submission
* A user can only vote once for a submission
* A user can only vote once for an improvement to a submission
* Total votes is an accumulation of submission and improvements votes
* Users should be rewarded for voting
* Users should be able to comment on a submission
* Users should be able to view existing submissions
* Submissions should be searchable
* Submissions should be sortable by popularity, date or number of views
* Submission should be categorised
* The user interface should be attractive and easy to use/navigate
* The users participation should be encouraged via gamification
* An administrator should be able to edit or delete submissions
* An administrator should be able to create user profiles and reset passwords
