/**
 * containers.js
 * 
 * Various storage classes used by the client of LevelledUpCurtin
 */

/**
 * The user container object
 */
function User(username, password, email, score)
{
    this.username = username;
    this.password = password;
    this.email = email;
    this.score = score;
};

/**
 * The submission container object
 */
function Submission(id, title, subtitle, user, date, category, url, averageVote, views, userScore, suggestions) 
{
	this.id = id;
	this.title = title;
    this.subtitle = subtitle;
	this.user = user;
	this.date = date;
	this.category = category;
	this.url = url;
    this.averageVote = averageVote;
    this.views = Number(views);
    this.userScore = userScore;
	this.suggestions = suggestions;
	
	if(this.url && this.url.substring(0, 4) != "http")
		this.url = "http://" + this.url;
}

/**
 * Counts the number of suggestions in a submission
 * 
 * @return the number of suggestions
 */
Submission.prototype.countComments = function()
{
	var count = this.suggestions.length;
    for(var index =0; index < this.suggestions.length; index++)
    {
        count += this.suggestions[index].countComments();
    }
    return count;
}

/**
 * Gets a URL descriptor for a webpage
 * 
 * @return the URL descriptor
 */
Submission.prototype.getUrlDescriptor = function()
{
	$.ajax({
		url: 'http://opengraph.io/api/1.0/site/' + encodeURI(this.url), 	
		success: function(data) 
				{
					this.descriptor = data.hybridGraph.title + " - " + data.hybridGraph.description;
				}.bind(this),
		async: false
	});
	return this.descriptor;
}

/**
 * Gets the average vote for a submission
 * 
 * @return the average vote
 */
Submission.prototype.getAvgScore = function()
{
	return this.averageVote + 1;
}

/**
 * Gets a time string for when a submission was posted
 * 
 * @return the time string
 */
Submission.prototype.getTimeString = function()
{
	var timeStr = this.date.getDate() + '/'
		+  (this.date.getMonth() + 1) + '/'
		+  this.date.getFullYear() + ' - ';
		
	if(this.date.getHours() < 10) timeStr += '0'
	timeStr +=  this.date.getHours() + ':'
	if(this.date.getMinutes() < 10) timeStr += '0'
	timeStr +=  this.date.getMinutes();
	
	return timeStr;
}

/**
 * Looks for a suggestion with the provided id
 * 
 * @param id the id of the suggestion
 * @return the suggestion (if found)
 */
Submission.prototype.getSuggestion = function(id)
{
	var suggestion, index = 0;
	while(suggestion == undefined && index < this.suggestions.length)
	{
		suggestion = this.suggestions[index].getSuggestion(id);
		index++;
	}
	return suggestion;
}

/**
 * Validates whether a submission links an image
 * 
 * @return true if it does
 */
Submission.prototype.isImage = function()
{
	var img = false;
	if(this.url && (/\.(jpg|gif|png)$/.test(this.url)))
		img = true;
	return img;	
}

/**
 * Gets an image for linked content
 * 
 * @return the link to the image
 */
Submission.prototype.getImage = function()
{
    var imgUrl = this.url;
    if(!this.isImage())
    {
        var urlParts = this.url.split("/");
        imgUrl = urlParts[0] + "//" + urlParts[2] + "/favicon.ico"
    } 
    if(this.youtubeID() != false)
    {
        imgUrl = "http://img.youtube.com/vi/"+this.youtubeID()+"/0.jpg"
    }
    return imgUrl;
}

/**
 * Gets the youtube id for linked content
 * 
 * @return the youtube id
 */
Submission.prototype.youtubeID = function()
{
	var videoID;
	if(this.url != undefined)
	{
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = this.url.match(regExp);
		videoID = (match&&match[7].length==11)? match[7] : false;
	}
	else videoID = false;
	return videoID;
}

/**
 * Checks if a submission is valud
 * 
 * @return true if valid
 */
Submission.prototype.valid = function()
{
    var valid = true;
    if(this.title == "" || this.subtitle == "")
        valid = false;
    return valid;
}

/**
 * Gets a string representing the number of views
 * 
 * @return the view string
 */
Submission.prototype.getViews = function()
{
    var viewString;     
    switch(true)
    {
        case (this.views > 1000000):
            viewString = (this.views/1000000).toFixed(2) + "M";
            break;
        case (this.views > 1000):
            viewString = (this.views/1000).toFixed(2) + "k";
            break;
        default:
            viewString = this.views;
    }
    return viewString;
}

/******************************************************************************
 * Container class for suggestion posts on submissions. This should be sorted
 * by parentId (The parent submission) and then by date after that. Replies are
 * an array of other suggestions. url is an array of urls in the message.
 *****************************************************************************/
function Suggestion (id, parentId, replies, title, message, date, url, rating
                     ,user) {
    this.getId = function() {
        return this.id;
    };
    
    this.setId = function(newId) {
        try {
            if (newId == undefined) {
                this.id = 0;
            }
            else if (newId >=0) {
                this.id = newId;
            }
            else throw "id must be >= 0";
        }
        catch (err) {
            console.log("setParentId Error: " + err);
        }
    };

    this.getParentId = function() {
        return this.parentId;
    };
    
    this.setParentId = function(newParent) {
        try {
            if (newParent == undefined) {
                this.parentId = 0;
            }
            else if (newParent >= 0) {
                this.parentId = newParent;
            } 
            else throw "Parent id must be >= 0";
        }
        catch (err) {
            console.log("setParentId Error: " + err);
        }
    };
    
    this.getReplies = function() {
        var rep = null;
        
        if (this.replies != null) {
            rep = this.replies;
        }
        
        return rep;
    };
    
    this.setReplies = function(newReplies) {
        try {
            if (newReplies == undefined) {
                this.replies = [];
            }
            else if (newReplies instanceof Array) {
                this.replies = newReplies;
            } 
            else throw "Array needed as argument";
        }
        catch (err) {
            console.log("Error setting replies: " + err);
        }
    }

    this.getTitle = function() {
        return this.title;
    };

    this.setTitle = function(newTitle) {
        if (newTitle == undefined) {
            this.title = "";
        }
        else {
            this.title = newTitle;
        }
    };

    this.getMessage = function() {
        var msg = null;
        
        if (this.notBlank(this.message)) {
            msg = this.message;
        }
        
        return msg;
    };

    this.setMessage = function(newMessage) {
        if (newMessage == undefined) {
            this.message = "default";
        }
        else if (this.notBlank(newMessage)) {
            this.message = newMessage;
        }
    };

    this.getDate = function() {
        return this.date;
    };

    this.setDate = function(newDate) {
        try {
            if (newDate == undefined) {
                this.date = new Date();
            }
            else if (newDate instanceof Date) {
                this.date = newDate;
            }
            else throw "Suggestion date must an instance of Date object";
        }
        catch (err) {
            console.log("setDate Error: " + err);
        }
    };

    this.getUrl = function() {
        if (this.url === undefined) {
            url = "";
        }
        
        return this.url;
    };

    this.setUrl = function(newUrl) {
        if (newUrl != undefined) {
            this.url = newUrl;
        }
        else {
            this.url = "";
        }
    };

    this.getRating = function() {
        return this.rating + 1;
    };

    this.setRating = function(newRating) {
        if (newRating == undefined) {
            this.rating = -1;
        }
        if (newRating <= 4 && newRating >= -1) {
            this.rating = newRating;
        }
    };
    
    this.getUser = function() {
        return this.user;
    };
    
    this.setUser = function(newUser) {
        if (newUser == undefined) {
            this.user = "N/A";
        }
        else {
            this.user = newUser;
        }
    };
    
   this.getUserScore = function() {
        return this.userScore;
    };
    
    this.setUserScore = function(newScore) {
        if (newScore == undefined) {
            this.userScore = "0";
        }
        else {
            this.userScore = newScore;
        }
    };
    
    /**************************************************************************
     * Date format is d/m/yyyy-hh:mm hours in 24 hour time.
     * getMonth() is between 0 - 11 so + 1 to get acutal month.
     *************************************************************************/
    this.getTimeString = function() {
		var timeStr = this.date.getDate() + '/'
			+  (this.date.getMonth() + 1) + '/'
			+  this.date.getFullYear() + ' - ';
		if(this.date.getHours() < 10) timeStr += '0'
		timeStr +=  this.date.getHours() + ':'
		if(this.date.getMinutes() < 10) timeStr += '0'
		timeStr +=  this.date.getMinutes();
		return timeStr;
    };
    
    this.countComments = function()
    {
        var count = this.replies.length;
        for(var index = 0; index < this.replies.length; index++)
        {
            count += this.replies[index].countComments();
        }
        return count;
    }
    
    this.notBlank = function(str) {
        var regex = /\S/;
        var valid = false;
        
        if (regex.test(str)) {
            valid = true;
        }
        else throw "data can't be blank";
        
        return valid;
    };
	
    this.getSuggestion = function(id) {
	var suggestion, index = 0;

	if(this.id == id) suggestion = this;
	else
	{
            while(suggestion == undefined && index < this.replies.length)
	    {
		suggestion = this.replies[index].getSuggestion(id);
		index++;
	    }
	}
	    return suggestion;	
    };
    
    this.setId(id);
    this.setParentId(parentId);
    this.setReplies(replies);
    this.setTitle(title);
    this.setMessage(message);
    this.setDate(date);
    this.setUrl(url);
    this.setUser(user);
}

function Vote (postId, rating) {
    this.setPostId = function (newPostId) {
        this.postId = newPostId;
    }

    this.getPostId = function () {
        return this.postId;
    }

    this.setRating = function (newRating) {
        this.rating = newRating
    }

    this.getRating = function () {
        return this.rating;
    }

    this.setPostId(postId);
    this.setRating(rating);
}
