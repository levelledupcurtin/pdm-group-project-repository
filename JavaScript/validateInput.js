/*****************************************************************************
/*Written by: Jordan
/*Date: 18/03/2016
/*Purpose: Provide various reusable input validation functions for the
/*         levelledUpCurtin project.
/*****************************************************************************/

var AccountValidation = {
    validateRegForm: function(){
        var valid =
        this.validateUsername($("input[name=usernameInput]").val());
        
        if (valid) { 
            valid = 
            this.validatePassword($("input[name=passwordInput]").val());
            
            if (valid) {
                valid = 
                this.validateEmail($("input[name=emailInput]").val());
            }
        }
        
        return valid;
    },

    validateLoginForm: function(){    
        var valid =
        this.validateUsername($("input[name=usernameInput]").val());
        
        if (valid) { 
            valid = 
            this.validatePassword($("input[name=passwordInput]").val());
        }
        
        return valid;
    },

    /*****************************************************************************
    /*Ensure input username has 3 <= length <= 16 as agreed upon by the 
    /*levelledUpCurtin team. It was also decided a usernames should begin with a
    /*letter.
    /*****************************************************************************/
    validateUsername: function(username){
        var valid = true;
        
        try{
            if (typeof username !== "string" && typeof username !== 
            "object") throw "Invalid Data Type\n";
            
            if (!Number.isNaN(parseInt(username.charAt(0)))) throw "First \
            character must be a letter\n";
            
            if (3 > username.length || username.length > 16) throw "Username Length \
            must be between 3 and 16 characters\n";
        } catch (err) {
            valid = false;
            throw err;
        }
        
        return valid;
    },

    /******************************************************************************
    /*Ensure password has 8 <= length <= 255, a range decided by the team. The
    /*decision not to force capitals, numbers etc is due to way constraints cause
    /*users to either make lazy passwords or to forget the complex passwords they
    /*create.
    /*ie if they were using john as a password and a number was forced they may
    /*choose john1 which is effectively as weak.
    /*
    /*It will therefore be advised but not required that users include mixed case
    /*and numbers.
    /*****************************************************************************/
    validatePassword: function(pWord){
        var valid = true;
        
        try {
            if (typeof pWord !== "string" && typeof pWord !== 
            "object") throw "Invalid Data Type\n";
            
            if (8 > pWord.length || pWord.length > 255) throw "Password Length must be\
            between 8 and 255 characters\n"
        } catch (err) {
            valid = false;
            throw err;
        }
        
        return valid;
    },

    /******************************************************************************
    /*Ensure that the email provided is in the valid email format of
    /*name@domain. 
    /*
    /*Email length should not be greater than 255
    /*characters due to database constraints. The length must be at least 3 to meet
    /*name@domain format ie "a@b".
    /*
    /*Only testing for name@domain format (and not what characters are either side)
    /*is done for two reasons. HTML5 email input type already validates email thus
    /*supporting browsers don't need this validation. The second is that even if
    /*the email is proven to be valid format. It does not necessarily exits. For
    /*that reason the email will be validated with a verification link sent to the
    /*provided address.
    /*****************************************************************************/
    validateEmail: function(email){
        var valid = true;
        var reg = /\S+@\S+/;
        
        try {
            if (typeof email !== "string" && typeof email !== 
            "object") throw "Invalid Data Type\n";
            
            if (3 > email.length || email.length > 255) throw "Email Length must be \
            between 3 and 255 characters\n";
            
            if(!reg.test(email)) throw "Not in the valid name@domain format"
        }
        catch (err) {
            valid = false;        
            throw err;
        }
        
        return valid;
    }
}