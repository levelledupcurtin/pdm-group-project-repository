/******************************************************************************
 * Written By: Jordan
 * Date: 08/04/2106
 * Purpose: Send on new submissions to the server. Performs some basic
 * validation on the suggestion before it is sent.
 *****************************************************************************/

$(document).ready(function() {
	ui.onSuggestion(SuggestionHandler.sendSuggestion.bind(SuggestionHandler));
    ui.onVote(SuggestionHandler.sendUserVote);
})

var SuggestionHandler = {
    //Takes in a Suggestion object only. Converts to JSON object internally.
    sendSuggestion: function(suggestion, parentSuggestion) {
        var request = new XMLHttpRequest();

        try {
            if (this.isValidSuggestion(suggestion)) {
                if (parentSuggestion == undefined) {
                    request.open("POST", "/php/suggestion_acquire.php", true);
                    request.send('{"singleSuggestion": ' + JSON.stringify(suggestion) + '}');
                    
                    SubmissionController.RetrieveSingle(ui.getPostId());
                }
                else if (this.isValidSuggestion(parentSuggestion)) {
                    var jsonString = '{"suggestion": '
                                     + JSON.stringify(suggestion)
                                     + ', "parentSuggestion": '
                                     + JSON.stringify(parentSuggestion) + '}';

                    request.open("POST", "/php/suggestion_acquire.php", true);
                    request.send(jsonString);

                    SubmissionController.RetrieveSingle(ui.getPostId());
                }
                else throw "parentSuggestion must be a suggestion object " +
                           "or undefined";
            }
            else throw "suggestion must be a Suggestion object";
        }
        catch (err){
            console.log("Error submitting suggestion: " + err);
        }
    },

    /**************************************************************************
     * Make sure the an object is of type suggestion and the message is not
     * empty / contains more than just blank space.
     *************************************************************************/
    isValidSuggestion: function(suggestion) {
        var isValid = false;
        try {
            if (suggestion instanceof Suggestion) {
                if (suggestion.getMessage() != null) {
                    isValid = true;
                }
            }
            else throw ("Not valid suggestion type.");
        }
        catch (err) {
            //console.log("Suggestion error: " + err);
        }

        return isValid;
    },

    /**************************************************************************
     * Used to take in a vote object and send it the the server for storage.
     *************************************************************************/
    sendUserVote: function (suggestion, voteVal) {
        if (SuggestionHandler.isValidSuggestion(suggestion))
        {
            var request = new XMLHttpRequest();
            var vote = new Vote(suggestion.id, voteVal);
            $.post("/php/suggestion_acquire.php",  JSON.stringify(vote),
            function(data)
            {
                if (data === true)
                {
                    console.log("Suggestion vote changed in database");
                }
                else
                {
                    console.log("Suggestion vote not changed in database");
                }
                SubmissionController.RetrieveSingle(ui.getPostId());
            }, "json");
        }
        else
        {
            SubmissionController.ChangeVote(suggestion, voteVal);
        }
    },

    /**************************************************************************
     * Checks if the parameter a vote object and that is contains all values.
     *************************************************************************/
    isValidVote: function (vote) {
        var isValid = false;

        try {
            if (vote instanceof Vote) {
                if (vote.getPostId() >= 0 &&
                    (vote.getRating() >= 0 && vote.getRating() <= 5)) {
                    isValid = true;
                }
                else throw ("Vote has invalid values");
            }
            else throw ("Object provided is not a vote");

            return isValid;
        }
        catch (err) {
            console.log("Invalid Vote: " + err);
        }
    }
}
