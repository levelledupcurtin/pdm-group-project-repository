/*******************************************************************************
* Purpose: To provided functions for handling operations required for
*          categorising submission including sending new categories to the
*          database and retrieving all categories from a database.
******************************************************************************/

var CategoryHandler = {
    /**************************************************************************
    * Sends a new category of submissions to the database. Only an admin should
    * be able to add a category. No users are allowed to add a new category.
    * Users may only post under existing categories.
    **************************************************************************/
    addNewCategory: function(category) {
        var request = new XMLHttpRequest();

        if (CategoryHandler.validateCategory(category)) {
            request.open("POST","/php/categories.php",true);
            request.send(category);
        }
    },

    /**************************************************************************
    * Makes sure a category is not blank and does not exceed INSERTNUM
    * characters.
    **************************************************************************/
    validateCategory: function(category) {
        var isValid = false;

        try {
            if (typeof category === 'string' && category.length > 0 &&
                category.length <= 255) {

                if (CategoryHandler.notBlank(category)) {
                    isValid = true;
                } else throw "Category can't be blank";
            } else throw "Category must be a string";
        }
        catch (err) {
            console.log("Invalid Category: " + err);
        }

        return isValid;
    },

    /**************************************************************************
    * Makes sure that the provided string is not blank
    **************************************************************************/
    notBlank: function(str) {
        var regex = /\S/;
        var valid = false;

        if (regex.test(str)) {
            valid = true;
        }

        return valid;
    },

    /**************************************************************************
    * Sends a request to retrieve all categories from the database. The data
    * returned will be an array of strings containing category names.
    **************************************************************************/
    retrieveCategories: function(callback) {

        $.post("/php/categories.php", "get_array_categories",
        function(categories) {
            callback(categories);
        }, "json");
    }
}
