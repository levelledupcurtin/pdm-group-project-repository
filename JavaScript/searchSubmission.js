/******************************************************************************
* Used to search for a phrase in the title, subtitle or url of a submission and
* return all submissions that match the phrase.
******************************************************************************/
function searchSubmissions (submissions, phrase) {
    var matches = [];
    
    try {
        if (submissions instanceof Array) {
            if (typeof phrase == typeof "") {
                for (ii = 0; ii < submissions.length; ii++) {
                    if (typeof submissions[ii].title == typeof "" &&
                        submissions[ii].title.search(new RegExp(phrase, "i")) > -1) {

                        matches.push(submissions[ii]);
                    }
                    else if (typeof submissions[ii].subtitle == typeof "" &&
                             submissions[ii].subtitle.search(new RegExp(phrase, "i")) > -1) {

                        matches.push(submissions[ii]);
                    }
                    else if (typeof submissions[ii].url == typeof "" &&
                             submissions[ii].url.search(new RegExp(phrase, "i")) > -1) {
                    
                        matches.push(submissions[ii]);
                    }
                }    
            } else throw ("Didn't receive a string for phrase.");
        } else throw ("Didn't receive an array for submissions.");
    }
    catch (err) {
        console.log("Error searching submissions: " + err);
    }
    
    return matches;
}
