/******************************************************************************
 * Written by: Jordan
 * Date: 08/04/2016
 * Purpose: Implement functions for searching through text and producing a
 *          a result.
 *****************************************************************************/
 
 var StringSearcher = {
     //Finds all urls in a string and gives them back in array form
    findUrls: function(str) {
        var urls;
        var regex = /https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}/g;
        
        return urls = str.match(regex);
    },
    
    //Finds a date in the format d/m/yyyy-h:m and returns a date object
    findDate: function(str) {
        var dateObj = null;
        var date;
        var time;
        var temp;
        var regex = /\d+[/]\d+[/]\d+[-]\d+[:]\d/;
        
        try {
            if (regex.test(str)) {
            temp = str.split("-");
            date = temp[0].split("/");
            time = temp[1].split(":");
            
            dateObj = new Date(date[2], date[1], date[0], time[0], time[1]);
            }
            else throw "date string must be d/m/yyyy-h:m";
        }
        catch (err) {
            console.log("findDate Error: " + err);
        }
        
        return dateObj;
    }
 }