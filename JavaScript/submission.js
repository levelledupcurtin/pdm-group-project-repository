$(document).ready(function()
{        
	if(typeof ui.setPosts == 'function') //We are on main page
	{
        CategoryHandler.retrieveCategories(ui.setCategories.bind(ui));
		SubmissionController.Retrieve(ui.getCategory());
		ui.onCategoryChange(SubmissionController.Retrieve);
	}
	else if(typeof ui.setPost == 'function') //We are on /post page, need to set the post
	{
		var id = ui.getPostId();
        SubmissionController.RetrieveSingle(id, true);
	}
    ui.onNewPost(SubmissionController.Add);
    ui.onVote(SubmissionController.ChangeVote);
});

var SubmissionController =
{    
    /*
        Checks that a Submission Field has content and isn't just whitespace
    */
    ValidateField: function(field)
    {
        var valid = true;
        
        if (field.length <= 0 || !field.trim())
        {
            valid = false;
        }
        
        return valid;
    },
    
    /*
        Validates the User Input Submission Fields
    */
    Validate: function(submission)
    {
        return (SubmissionController.ValidateField(submission.title) && SubmissionController.ValidateField(submission.subtitle));
    },
    
    /*
        Checks the new Submission before sending by POST to submission.php
    */
    Add: function(submission)
    {
        if (SubmissionController.Validate(submission))
        {
            if (submission.category === undefined)
            {
                submission.category = ui.getCategory();
            }

            $.post("/php/submission_acquire.php",
            {
                "task": "insert_submission",
                "title": submission.title,
                "subtitle": submission.subtitle,
                "category": submission.category,
                "url": submission.url
            },
            function(data)
            {
                if (data === true)
                {
                    console.log("Submission added to database");
                }
                else
                {
                    console.log("Error Adding Submission to database");
                }
            }, "json");
            
            SubmissionController.Retrieve($("select[name='menuCategorySelector']").val());
        }
    },
    
    /*
        Imports an Object containg a Time Field (String format)
        
        Returns a fully constructed Date object. 
    */
    GetDate: function(submission)
    {      
        var dateComponents = submission.Time.date.split(/[- :]/);
        var date = new Date(dateComponents[0], dateComponents[1]-1, dateComponents[2], dateComponents[3], dateComponents[4], dateComponents[5]);
        
        return date;  
    },
    
    /*
        Imports the Parent Submission and a Suggestion (both non JS Objects)
        
        Recursive Method which constructs a Suggestion Object and its replies (also Suggestion Objects)
        
        Returns a Suggestion Object with all its nested Replies. 
    */
    GetSuggestion: function(parentSubmission, suggestion)
    {
        var suggestionObj = new Suggestion();
        suggestionObj.setId(suggestion.SuggestionID);
        suggestionObj.setParentId(suggestion.ReplySuggestionId);
        
        var replies = [];
        $.each(parentSubmission.Suggestions, function(index, reply)
        {
            if (reply.ReplySuggestionId === suggestion.SuggestionID)
            {
                replies.push(SubmissionController.GetSuggestion(parentSubmission, reply));
            }
        });
        suggestionObj.setReplies(replies);
        suggestionObj.setTitle(suggestion.Title);
        suggestionObj.setMessage(suggestion.Comment);
        suggestionObj.setDate(SubmissionController.GetDate(suggestion));
        suggestionObj.setUrl(suggestion.URL);
        suggestionObj.setRating(suggestion.Rating);
        suggestionObj.setUser(suggestion.Username);
        suggestionObj.setUserScore(suggestion.UserScore);
        
        return suggestionObj;
    },
    
    /*
        Imports an Object containing a Suggestions Field (an array of Strings)
         
        Returns an array of fully constructed Suggestion Objects at the highest level to the Submission.
    */
    GetSuggestions: function(submission)
    {
        var suggestions = [];
        
        $.each(submission.Suggestions, function(index, suggestion)
        {
            var suggestionObj = SubmissionController.GetSuggestion(submission, suggestion);
            
            if (suggestionObj.getParentId() === 0)
            {
                suggestions.push(suggestionObj);
            }
        });
        
        return suggestions;
    },
    
    /*
        Imports an array of Strings (from a request to the php) 
        
        Returns a fully constructed Submission Object.
    */
    GetSubmission: function(post)
    {
        var date = SubmissionController.GetDate(post);
        var suggestions = SubmissionController.GetSuggestions(post);
        
        var submission = new Submission(post.SubmissionID, post.Title, post.Comment, post.Username, date, post.Category, post.URL, post.VoteAverage, post.Views, post.UserScore, suggestions);
        return submission;
    },
    
    /*
        Sends a request to submission_acquire.php with the current selected category.
        
        The data returned by the script will contain an array of submissions, which is displayed.
    */
    Retrieve: function(category)
    {        
        $.post("/php/submission_acquire.php",
        {
            "task": "get_submission_category",
            "category": category
        },
        function(data)
        {
            var posts = [];
            $.each(data, function(index, post)
            {
                posts[index] = SubmissionController.GetSubmission(post);
            });
            ui.setPosts(posts);
        }, "json");
    },
    
    /*
        Sends a request to submission_acquire.php with an ID, to get a Single Submission.
        
        The data returned is an array of Strings which contains information for the Submission.
        The Submission is displayed along with all its data.
    */
    RetrieveSingle: function(id, countView)
    {
        $.post("/php/submission_acquire.php",
        {
            "task": "get_submission",
            "id": id,
            "view": countView
        },
        function(data)
        {
            $.each(data, function(index, post)
            {                
                if (post.SubmissionID === id)
                {
                    ui.setPost(SubmissionController.GetSubmission(post));
                }
            });
        }, "json");
    },
    
    /*
        Sends a request to submission_acquire.php with an ID and a vote, to change the vote of the Submission.
    */
    ChangeVote: function(submission, vote)
    {
        if (submission instanceof Submission)
        {
            var voteObj = new Vote(submission.id, vote); 
            $.post("/php/submission_acquire.php",
            {
                "task": "change_vote",
                "id": voteObj.getPostId(),
                "vote": voteObj.getRating()
            },
            function(data)
            {
                if (data === true)
                {
                    console.log("Submission vote changed in database");
                }
                else
                {
                    console.log("Submission vote not changed in database");
                }
               	if(typeof ui.setPosts == 'function') //We are on main page
                    SubmissionController.Retrieve(ui.getCategory());
                else
                    SubmissionController.RetrieveSingle(ui.getPostId(), false);
            }, "json");
        }
    }
};