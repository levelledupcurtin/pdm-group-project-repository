/**
 * suggestionui.js
 * 
 * Interactivity for the suggestion display of LevelledUpCurtin
 */

/**
 * Set up the suggestion page
 */
UI.prototype.configure = function()
{
	$('#commentClose').click(function()
	{
		$("#newComment").css("display", "none");
	});	
}

/**
 * Extension of submission container to convert to HTML
 */
Submission.prototype.toDOM = function()
{
	var div = '';

		div += '<div class="post" id="'+this.id+'">';
		div +='<a href="#newComment"><div class="addComment lightText"><i class="fa fa-plus"></i></div></a>'
			+ '	<div class="stars lightText">';
	for(var count = 0; count < 5; count++)
	{
		if(this.getAvgScore() >= count+1)
			div += '<i class="hoverable fa fa-star" id="'+count+' -1"></i>';
		else if(Math.ceil(this.getAvgScore()) >= count+1)
			div += '<i class="hoverable fa fa-star-half-o" id="'+count+' -1"></i>';
		else
			div += '<i class="hoverable fa fa-star-o" id="'+count+' -1"></i>';
	}
		div += '	</div>';
		div	+= '	<div class="postTitle">'+this.title+'</div>'
			+  '	<div class="postSubtitle">'+this.subtitle+'</div>'
			
	if(this.youtubeID())
		div += '<div class="postLink"><iframe class="postLinkedContent" width="420" height="315" src="http://www.youtube.com/embed/'+this.youtubeID()+'"> </iframe></div>'
	else if(this.url)
	{
		if(this.isImage())
			div += '<div class="postLink"><a href="'+this.url+'"><img class="postLinkedContent" src="'+this.getImage()+'"></a></div>';
		else
			div += '<div class="postLink"><a href="'+this.url+'"><img class="postLinkedContent" src="'+this.getImage()+'">'+this.getUrlDescriptor()+'</a></div>';
	}
		div	+=  '	<div class="postStats">'
			+  '		<div class="lightBar lightBackground">'
			+  '			<div class="postUser contrastText">'+this.user+" - <i>"+this.userScore+'</i></div>'
			+  '			<div class="postTime contrastText">'+this.getTimeString()+'</div>'
			+  '			<div class="postCategory contrastText">'+this.category+'</div>'	
			+  '		</div>'
			+  '		<div class="darkBar darkBackground contrastText">'
			+  '			'+this.countComments()+'&nbsp;<i class="fa fa-comment"></i>&nbsp;&nbsp;'
			+  '            '+this.getViews()+'&nbsp;<i class="fa fa-eye" aria-hidden="true"></i>'
			+  '		</div>'
			+  '</div></div></div>';
	return div;
}

/**
 * Extension of suggestion container to convert to HTML
 */
Suggestion.prototype.toDOM = function()
{
	var div = '<div class="comment" id="sug '+this.getId()+'">'
			+ '<a href="#newComment">'
			+ '<div class="addComment lightText" id="add'+this.getId()+'">'
			+ '<i class="fa fa-plus"></i>'
			+ '</div></a>'
			+ '	<div class="stars lightText">';
	for(var count = 0; count < 5; count++)
	{
		if(this.getRating() >= count+1)
			div += '<i class="hoverable fa fa-star" id="'+count+' '+this.getId()+'"></i>';
		else if(Math.ceil(this.getRating()) >= count+1)
			div += '<i class="hoverable fa fa-star-half-o" id="'+count+' '+this.getId()+'"></i>';
		else
			div += '<i class="hoverable fa fa-star-o" id="'+count+' '+this.getId()+'"></i>';
	}
	div +=  '	</div>'
		+	'	<div class="commentText">'+this.getMessage()+'</div>'
		+	'	<div class="lightBar lightBackground">'
		+	'   	<div class="commentUser contrastText">'+this.getUser()+" - <i>"+this.getUserScore()+'</i></div>'
		+	'		<div class="commentTime contrastText">'+this.getTimeString()+'</div>'
		+	'	</div></div>';
		
	var subCommentString = '';
	for(var index = 0; index < this.getReplies().length; index++)
		subCommentString += this.getReplies()[index].toDOM();
	
	div +=	'<div class="commentReply" id="'+this.getId()+' reply">'	
		+	subCommentString
		+	'</div>';
		
		return div;
}

/**
 * Sets the current submission to display suggestions for
 * 
 * @param post the submission to display
 */
UI.prototype.setPost = function(post)
{
	this.post = post;
	$('#postDisplay').html("");
	$('#postDisplay').append(post.toDOM());
	for(var index = 0; index < post.suggestions.length; index++)
		$('#postDisplay').append(post.suggestions[index].toDOM());
	$('.addComment').click(this.setReply.bind(this));
	this.setCategory(post.category);
	this.setTheme();
	$('.stars .fa-star, .stars .fa-star-half-o, .stars .fa-star-o').click(this.newVote.bind(this));	
}

/**
 * Sets the new suggestion reply based on what was selected to reply to
 * 
 * @param event the jQuery event
 */
UI.prototype.setReply = function(event)
{
	var id = $(event.currentTarget).attr('id');
	if(id != undefined)
	{
		id = id.substring(3);
		this.reply = this.post.getSuggestion(id);
	}
	else this.reply = undefined;
	if(this.loggedIn)
		$("#newComment").css("display", "block");
	else
		alert("You need to be logged in to comment");
}

/**
 * Sets the callback for a new suggestion
 * 
 * @param callback the suggestion callback
 */
UI.prototype.onSuggestion = function(callback)
{
	this.suggestionCallback = callback;
	$('#newCommentOk').click(function()
	{
		this.newSuggestion();
		$("#newComment").css("display", "none");
	}.bind(this));
}

/**
 * Creates a new suggestion
 * 
 * @param event the jQuery event
 */
UI.prototype.newSuggestion = function(event)
{
	var suggestion = new Suggestion();
	try
	{
		suggestion.setParentId(this.post.id);
		suggestion.setMessage($('textarea[name="newComment"]').val());
		$('textarea[name="newComment"]').val("");
		suggestion.setDate(new Date());
		if(this.loggedIn)
			this.suggestionCallback(suggestion, this.reply);
		else
			alert("You cannot make a suggestion unless you're logged in");
	}
	catch(err) { alert("One or more required fields is missing data"); } 
}

/**
 * Gets the id of the current submission
 * 
 * @return the id of the current submission
 */
UI.prototype.getPostId = function()
{
	return window.location.search.substr(1);
}

/**
 * Handles the voting process for suggestions
 * 
 * @param event the jQuery event
 */
UI.prototype.newVote = function(event)
{
	var values = $(event.currentTarget).attr('id').split(" ");
	if(this.loggedIn)
	{
		if(values[1] == '-1')
		{
			if(this.voteCallback != undefined)
				this.voteCallback(this.post, values[0]);	
		}
		else
		{
			var suggestion = this.post.getSuggestion(values[1]);
			if(suggestion != undefined && this.voteCallback != undefined)
			{
				this.voteCallback(suggestion, values[0]);
			}
		}
	}
	else 
	alert("You cannot vote if you're not logged in");
}
