/******************************************************************************
 * Purpose: To implement useful sorting algorithms for the levelledupcurtin
 *          website.
 *****************************************************************************/

/******************************************************************************
 * Merge sort for sorting an array of objects by date. The objects in the array
 * should have a function called getDate() that returns a javascript Date
 * object. This has been used for ordering replies to each suggestion.
 *****************************************************************************/
function mergeSortDate(arr) {
    if (arr.length < 2)
        return arr;
 
    var middle = parseInt(arr.length / 2);
    var left   = arr.slice(0, middle);
    var right  = arr.slice(middle, arr.length);
 
    return mergeDate(mergeSortDate(left), mergeSortDate(right));
}

function mergeDate(left, right)
{
    var result = [];
 
    while (left.length && right.length) {
        if (left[0].getDate() <= right[0].getDate()) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
 
    while (left.length) {
        result.push(left.shift());
    }
 
    while (right.length) {
        result.push(right.shift());
    }
 
    return result;
}

/******************************************************************************
 * Merge sort for sorting an array of objects by date. The objects in the array
 * should have a field called date accessible by this.date that contains
 * javascript Date object. This has been used for ordering submissions.
 *****************************************************************************/
function sortSubmissionDate(arr) {
    if (arr.length < 2)
        return arr;
 
    var middle = parseInt(arr.length / 2);
    var left   = arr.slice(0, middle);
    var right  = arr.slice(middle, arr.length);
   
    return mergeSubmissionDates(sortSubmissionDate(left),
                            sortSubmissionDate(right));
}

function mergeSubmissionDates(left, right)
{
    var result = [];
 
    while (left.length && right.length) {
        if (left[0].date >= right[0].date) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
 
    while (left.length) {
        result.push(left.shift());
    }
 
    while (right.length) {
        result.push(right.shift());
    }
 
    return result;
}

/******************************************************************************
 * Used to sort submissions by popularity and if two popularities are the same
 * then sorting by date as a second criteria. Objects in the array need to have
 * a field called date that contains a javascript date object.
 *****************************************************************************/
function sortSubmissionPopularity(arr) {
    arr = sortSubmissionDate(arr);
    arr = mergesortSubmissionPopularity(arr);

    return arr;
}

function mergesortSubmissionPopularity(arr) {
    if (arr.length < 2)
        return arr;
 
    var middle = parseInt(arr.length / 2);
    var left   = arr.slice(0, middle);
    var right  = arr.slice(middle, arr.length);
 
    return mergePopularity(mergesortSubmissionPopularity(left),
                           mergesortSubmissionPopularity(right));
}

function mergePopularity(left, right)
{
    var result = [];
 
    while (left.length && right.length) {
        if (left[0].getAvgScore() >= right[0].getAvgScore()) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
 
    while (left.length) {
        result.push(left.shift());
    }
 
    while (right.length) {
        result.push(right.shift());
    }
 
    return result;
}

/******************************************************************************
 * Merge sort for sorting an array of objects by number of views.
 * The objects in the array should have a field called date accessible by this.
 * date that contains javascript Date object. If two view counts are equal then
 * date is used as the second sorting criteria. This has been used for ordering
 * submissions.
 *****************************************************************************/
function sortSubmissionViews(arr) {
    arr = sortSubmissionDate(arr);
    arr = mergesortSubmissionViews(arr);

    return arr;
}

function mergesortSubmissionViews(arr) {
    if (arr.length < 2)
        return arr;
 
    var middle = parseInt(arr.length / 2);
    var left   = arr.slice(0, middle);
    var right  = arr.slice(middle, arr.length);
 
    return mergeSubmissionViews(mergesortSubmissionViews(left),
                                mergesortSubmissionViews(right));
}

function mergeSubmissionViews(left, right) {
    var result = [];
 
    while (left.length && right.length) {
        if (left[0].views >= right[0].views) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
 
    while (left.length) {
        result.push(left.shift());
    }
 
    while (right.length) {
        result.push(right.shift());
    }
 
    return result;
}
