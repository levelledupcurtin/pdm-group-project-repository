$(document).ready(function()
{
    // Attach event handlers for the Click Event
    $("#registerUser").click(Registration.SendUserInfo); 
});

/*
    Returns a User Object from the information in the Input Form
*/
function GetUser() 
{
    var username = $("input[name=usernameInput]").val();
    var password = $("input[name=passwordInput]").val();
    var email = $("input[name=emailInput]").val();
    
    return new User(username, password, email);
};

/*
    Registration Object
*/
var Registration =
{
    /*
        Tries to validate the Registration Form.
        If it succeeds, a User Object is created (containing the user's information) and the User Object is sent by POST to the register script.
        
        In case of any errors, the "#responseDisplay" is updated to show an appropriate message.
    */
    SendUserInfo: function()
    {
        $("#responseDisplay").css("color", "red");
        
        try
        {
            if(AccountValidation.validateRegForm())
            {
                var user = GetUser();
                $.post("/php/register.php",
                {
                    "user":
                    {
                        "username": user.username,
                        "password": user.password,
                        "email": user.email
                    }
                },
                function(data)
                {
                    $("#responseDisplay").css("color", "red");
                
                    if (data["error"]["code"] === 1)        // GET_MSG
                    {
                        $("#responseDisplay").text("Invalid Request. Use POST."); 
                    }
                    else if (data["error"]["code"] === 2)   // BAD_JSON_MSG
                    {
                        $("#responseDisplay").text("Request did not send the correct fields.");                                
                    }
                    else if (data["error"]["code"] === 3)   // INVALID_CREDENTIALS_MSG
                    {
                        $("#responseDisplay").text("Incorrect Username or Password. Please try again.");
                    }
                    else if (data["error"]["code"] === 4)   // USER_EXISTS_MSG
                    {
                        $("#responseDisplay").text("A user with the username: " + "\"" + user.username + "\"" + " already exists. Please try another one.");
                    }
                    else if (data["error"]["code"] === 5)   // DB_ERROR_MSG
                    {
                        $("#responseDisplay").text("A Database error occured. Please try again later.");
                    }
                    else                                    // Success
                    { 
                        $("#responseDisplay").css("color", "green");
                        $("#responseDisplay").text("User Created");
						window.location.href = "/"; // Go to homepage
                    }
                
                    $("#responseDisplay").effect("bounce");            
                }, "json");
            }
        }
        catch(msg)
        {
            $("#responseDisplay").text(msg)
        }
        
        $("#responseDisplay").effect("bounce");  
    }
};

/*
    Login Object
*/
var Login =
{
    /*
        Tries to validate the Login Form.
        If it succeeds, a User Object is created (containing the user's information) and the User Object is sent by POST to the login script.
        
        In case of any errors, the "#responseDisplay" is updated to show an appropriate message.
    */
    SendUserInfo: function()
    {
        $("#responseDisplay").css("color", "red");
        $("#responseDisplay").text("");
        
        try
        {
            if (AccountValidation.validateLoginForm())
            {
                this.user = GetUser();
                $.post("/php/login.php",
                {
                    "user":
                    {
                        "username": this.user.username,
                        "password": this.user.password
                    }
                },
                function(data)
                {                
                    if (data["error"]["code"] === 1)        // GET_MSG
                    {
                        $("#responseDisplay").text("Invalid Request. Use POST."); 
                    }
                    else if (data["error"]["code"] === 2)   // BAD_JSON_MSG
                    {
                        $("#responseDisplay").text("Request did not send the correct fields.");                                
                    }
                    else if (data["error"]["code"] === 3)   // INVALID_CREDENTIALS_MSG
                    {
                        $("#responseDisplay").text("Incorrect Username or Password. Please try again.");
                    }
                    else                                    // Success
                    { 
                        $("#responseDisplay").text("");
                        Login.CheckUser();
                    }
                            
                }.bind(this), "json");
            }
        }
        catch(msg)
        {
            $("#responseDisplay").text(msg);
        }
 
    },
    
    Logout: function()
    {
        $.post("/php/login.php",
        {
            "task": "logout"
        },
        function(data)
        {
            if (data["successful"])
            {
		        ui.setUser();
            }
        }, "json");
    },
    
    CheckUser: function()
    {
        $.post("/php/login.php",
        {
            "task": "Check User Status"
        },
        function(data)
        {
            if (data["successful"])
            {
                ui.setUser(data["response"]["username"], data["response"]["score"]);
            }
            else
            {
                ui.setUser();
            }
        }, "json");
    }
};
