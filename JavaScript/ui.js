/**
 * ui.js
 * 
 * Interactivity for the general LevelledUpCurtin interface
 */

/**
 * The set of themes that can be applied
 */
var themes = 
{
	Blue: 
	{
		light: "rgb(29, 139, 241)",
		dark: "rgb(23, 107, 204)",
		contrast: "white"
	},
	Curtin: 
	{
		light: "rgb(0, 0, 0)",
		dark: "rgb(179, 129, 16)",
		contrast: "white"
	},
	Green: 
	{
		light: "rgb(81, 169, 85)",
		dark: "rgb(43, 120, 47)",
		contrast: "white"
	},
	Red: 
	{
		light: "rgb(221, 92, 93)",
		dark: "rgb(198, 36, 38)",
		contrast: "white"
	},
	Orange: 
	{
		light: "rgb(255, 146, 32)",
		dark: "rgb(242, 100, 3)",
		contrast: "white"
	},
	Purple: 
	{
		light: "rgb(151, 55, 171)",
		dark: "rgb(101, 28, 141)",
		contrast: "white"
	},
	Grey: 
	{
		light: "rgb(98, 122, 134)",
		dark: "rgb(53, 70, 79)",
		contrast: "white"
	},
	Zufallig:
	{
		light: ('#' + Math.random().toString(16).slice(2, 8).toUpperCase()),
		dark: ('#' + Math.random().toString(16).slice(2, 8).toUpperCase()),
		contrast: "white"
	}
}

/**
 * The main user inteface object
 */
function UI() 
{
	this.loggedIn = false;
	
	$("#loginButton, #registerButton").hide();
	
	// Load theme options into UI
	$.each(themes, function(key, element)
	{
		$("select[name='themes']").append($("<option>", {value:key, html:key}));
	});
	this.setTheme();
	
	// Theme change handler
	$("select[name='themes']").change(function()
	{
		this.setTheme($("select[name='themes']").val());
		Cookies.set('color', $("select[name='themes']").val());
	}.bind(this));
	// Login button handler
	$("#loginButton, #menuLoginButton").click(function()
	{
		$('#mobilemenu').css("display", "none");
		this.menuOpen = false;	
		if(this.loginVisible == undefined || !this.loginVisible)
		{
			this.loginVisible = true;
			$('#loginForm').css('display', 'block');
			$('input[name="usernameInput"]').focus();
		}
		else
			Login.SendUserInfo();
	}.bind(this));
	// Login dialog close
	$("#loginClose").click(function()
	{
		this.loginVisible = false;
		$('#loginForm').css('display', 'none');
	}.bind(this));
	// Enter key press for login
	$("input[name='passwordInput']").keypress(function(event)
	{
		if(event.keyCode == 13) Login.SendUserInfo();
	});

	// Get current login status
	Login.CheckUser();
	// Submission dialog button handlers
	$('#newOk').click(function()
	{
		this.newPost();
		$('#newPost').css("display", "none");
	}.bind(this));
	$('#newClose').click(function()
	{
		$('#newPost').css("display", "none");
	});
	
	$('#userLabel').hide();
	
	$('#loggedInBox').hide();
	$('#logoutButton, #menuLogoutButton').click(Login.Logout);
	
	// Individual UI config
	this.configure();
	// New suggestion button handler
	$('#addButton, #addMenuButton').click(function()
	{ 
		$('#mobilemenu').css("display", "none");
		this.menuOpen = false;	
		if(this.loggedIn)
			$('#newPost').css("display", "block"); 
		else
			alert("You need to be logged in to post");	
	}.bind(this));
	// Mobile menu handler
	$('#burger').click(function()
	{
		if(this.menuOpen == undefined || this.menuOpen == false)
		{
			$('#mobilemenu').css("display", "block");
			this.menuOpen = true;
		}
		else
		{
			$('#mobilemenu').css("display", "none");
			this.menuOpen = false;	
		}
				
	}.bind(this));
};


/**
 * Sets the UI to a particular theme
 * 
 * @param color specified theme
 */
UI.prototype.setTheme = function(color)
{
	// If no theme specified, get saved from cookies
	if(color == undefined)
		var color = Cookies.get('color');
	// Otherwise default to blue
	if(color == undefined) color = "Blue";
	$("select[name='themes']").val(color);
	var theme = themes[color];
	// Change CSS
	$('.darkBackground, .darkButton').css({"background":theme.dark});
	$('.darkText').css({"color":theme.dark});
	
	$('.darkButton').hover(function() 
	{
		$(this).css("background", theme.light);
	}, function()
	{
		$(this).css("background", theme.dark);
	});
	
	$('.lightBackground, .lightButton').css({"background":theme.light});
	$('.lightText, .hoverable').css({"color":theme.light});
	$('.lightInput input[type="text"]').css({"background":theme.light});
	
	$('.lightButton').hover(function() 
	{
		$(this).css("background", theme.dark);
	}, function()
	{
		$(this).css("background", theme.light);
	});
	
	$('.hoverable').hover(function() 
	{
		$(this).css("color", theme.dark);
	}, function()
	{
		$(this).css("color", theme.light);
	});
	
	$('.hoverable').click(function() 
	{
		$(this).css("color", "black");
	});
	
};

/**
 * Experimental randomly changing theme
 * 
 * @param time the duration to change on (in ms)
 */
UI.prototype.setThemeRepeatedly = function(time)
{
	themes.Zufallig.light = ('#' + Math.random().toString(16).slice(2, 8).toUpperCase());
	themes.Zufallig.dark = ('#' + Math.random().toString(16).slice(2, 8).toUpperCase());
	this.setTheme("Zufallig");
	if(time != undefined)
		setInterval(this.setThemeRepeatedly.bind(this), time);
}

/**
 * Set the callback for category changes
 * 
 * @param callback the category callback
 */
UI.prototype.onCategoryChange = function(callback)
{
	this.categoryCallback = callback;
}

/**
 * Get the current category
 * 
 * @return the current category
 */
UI.prototype.getCategory = function(callback)
{
	return $('.categoryName').first().text();
}

/**
 * Set the callback for new submissions
 * 
 * @param callback for new submissions
 */
UI.prototype.onNewPost = function(callback)
{
	this.newPostCallback = callback;
}

/**
 * Set the callback for new votes
 * 
 * @param callback for new votes
 */
UI.prototype.onVote = function(callback)
{
	this.voteCallback = callback;
}

/**
 * Sets the category
 * 
 * @param category the category to set
 */
UI.prototype.setCategory = function(category)
{
	$('#mobilemenu').css("display", "none");
	this.menuOpen = false;	
	Cookies.set('category', category);
	$('.categoryName').text(category);
	$(".categorySelector select").val(category);
	if("categoryCallback" in this)
		this.categoryCallback(category);
}

/**
 * Sets the list of selectable categories
 * 
 * @param categories the list of categories
 */
UI.prototype.setCategories = function(categories)
{
	$(".categorySelector select").html("");
	for(var index = 0; index < categories.length; index++)
	{
		$(".categorySelector select").append($("<option>", 
			{value: categories[index].Category, html: categories[index].Category}
		));
	}
	$(".categorySelector select").change(function(event)
	{
		this.setCategory($(event.currentTarget).val());
	}.bind(this));
	if(categories.length > 0) 
	{
		if(Cookies.get('category') != undefined)
			this.setCategory(Cookies.get('category'));
		else
			this.setCategory(categories[0].Category);
	}
}

/**
 * Handler for creating a new suggestion
 */
UI.prototype.newPost = function()
{
	post = new Submission();
	post.title = $('input[name="newTitle"]').val();
	$('input[name="newTitle"]').val("");
	post.subtitle = $('textarea[name="newSubtitle"]').val();
	$('textarea[name="newSubtitle"]').val("");
	post.url = $('input[name="newURL"]').val();
	$('input[name="newURL"]').val("");
	post.category = $("select[name='menuCategorySelector']").val();
	if("newPostCallback" in this)
	{
		if(this.loggedIn)
			if(post.valid())
				this.newPostCallback(post);
			else alert("You are missing a field");
		else alert("You cannot post if you're not logged in");
	}
}

/**
 * Sets the current user with a score in the UI
 * 
 * @param user the usernameInput
 * @param score the score
 */
UI.prototype.setUser = function(user, score)
{
	$('#loginForm').css('display', 'none');
	$("input[name=usernameInput]").val('');
	$("input[name=passwordInput]").val('');
	if(user == undefined)
	{
		this.loggedIn = false;
		this.loginVisible = false;
		$('#loginButton, #registerButton, #menuLoginButton, #menuRegisterButton').show();
		$('#loggedInBox, #menuLoggedInBox').hide();
	}
	else
	{
		this.loggedIn = true;
		$('#loginButton, #registerButton, #menuLoginButton, #menuRegisterButton').hide();
		$('#loggedInBox, #menuLoggedInBox').show();
		$('.userLabel').text(user);	
		$('.pointCounter').text(score);
	}
}

/**
 * Creates the UI when the page loads
 */
$(document).ready(function()
{
	ui = new UI();
});