/**
 * submissionui.js
 * 
 * Interactivity for the submission display of LevelledUpCurtin
 */

/**
 * Set up the submission page
 */
UI.prototype.configure = function()
{
	// Search Field
	$('#searchField input, #menuSearch input').keyup(this.search.bind(this));
	// Sorting
	$('#starSortButton').click(this.sortVotes.bind(this));
	$('#timeSortButton').click(this.sortTime.bind(this));
	$('#viewSortButton').click(this.sortViews.bind(this));
	
	this.sorting = sortSubmissionPopularity;
}

/**
 * Extension of submission container to convert to HTML
 */
Submission.prototype.toDOM = function()
{
	var div = '';
	if(!this.url) 
		div += '<div class="post" id="'+this.id+'">';
	else
		div += '<div class="imagePost post" id="'+this.id+'">'
		div += '	<div class="stars lightText">';
	for(var count = 0; count < 5; count++)
	{
		if(this.getAvgScore() >= count+1)
			div += '<i class="hoverable fa fa-star" id="'+count+' '+this.id+'"></i>';
		else if(Math.ceil(this.getAvgScore()) >= count+1)
			div += '<i class="hoverable fa fa-star-half-o" id="'+count+' '+this.id+'"></i>';
		else
			div += '<i class="hoverable fa fa-star-o" id="'+count+' '+this.id+'"></i>';
	}
		div += '	</div><a href="post?'+this.id+'">';
	if(this.url)
	{
		div += '	<img src="'+this.getImage()+'">';
	}
		div	+= '	<div class="postTitle">'+this.title+'</div>'
			+  '	<div class="postStats">'
			+  '		<div class="lightBar lightBackground">'
			+  '			<div class="postUser contrastText">'+this.user+" - <i>"+this.userScore+'</i></div>'
			+  '			<div class="postTime contrastText">'+this.getTimeString()+'</div>'
			+  '			<div class="postCategory contrastText">'+this.category+'</div>'	
			+  '		</div>'
			+  '		<div class="darkBar darkBackground contrastText">'
			+  '			'+this.countComments()+'&nbsp;<i class="fa fa-comment"></i>&nbsp;&nbsp;'
			+  '            '+this.getViews()+'&nbsp;<i class="fa fa-eye" aria-hidden="true"></i>'
			+  '		</div>'
			+  '</div></div></div></a>';
	return div;
}

/**
 * Sets an array of submissions to be displayed
 */
UI.prototype.setPosts = function(postArray)
{
	if(postArray != undefined)
		this.posts = postArray;
	this.displayPosts(this.posts);
}

/**
 * Displays an array of submissions
 */
UI.prototype.displayPosts = function(postArray)
{
	postArray = this.sorting(postArray);
	$('#postDisplay').html("");
	$.each(postArray, function(index, post)
	{
		$('#postDisplay').append(post.toDOM());
	});
	this.setTheme();
	$('.stars .fa-star, .stars .fa-star-half-o, .stars .fa-star-o').click(this.newVote.bind(this));	
}

/**
 * Handles the voting process for submissions
 * 
 * @param event the jQuery event
 */
UI.prototype.newVote = function(event)
{
	var values = $(event.currentTarget).attr('id').split(" ");
	var submission, index = 0;
	// Find submission
	while(submission == undefined && index < this.posts.length)
	{
		if(this.posts[index].id == values[1])
			submission = this.posts[index];
		index++;
	}
	if(submission != undefined && this.voteCallback != undefined)
	{
		if(this.loggedIn)
			this.voteCallback(submission, values[0]);
		else alert("You cannot vote if you're not logged in");
	}
		
}

/**
 * Handles the search event
 */
UI.prototype.search = function()
{
	var query = $('#searchField input').val() + $('#menuSearch input').val();
	if(query == "")
		this.displayPosts(this.posts);
	else
		this.displayPosts(searchSubmissions(this.posts, query));
}

/**
 * Sets sorting of submissions by votes
 */
UI.prototype.sortVotes = function()
{
	$('#starSortButton').removeClass("lightButton contrastText").addClass("darkButton contrastText");
	$('#timeSortButton').removeClass("darkButton contrastText").addClass("lightButton contrastText");
	$('#viewSortButton').removeClass("darkButton contrastText").addClass("lightButton contrastText");
	this.sorting = sortSubmissionPopularity;
	this.setPosts();
}

/**
 * Sets sorting of submissions by newest
 */
UI.prototype.sortTime = function()
{
	$('#timeSortButton').removeClass("lightButton contrastText").addClass("darkButton contrastText");
	$('#starSortButton').removeClass("darkButton contrastText").addClass("lightButton contrastText");
	$('#viewSortButton').removeClass("darkButton contrastText").addClass("lightButton contrastText");
	this.sorting = sortSubmissionDate;
	this.setPosts();
}

/**
 * Sets sorting of submissions by views
 */
UI.prototype.sortViews = function()
{
	$('#viewSortButton').removeClass("lightButton contrastText").addClass("darkButton contrastText");
	$('#starSortButton').removeClass("darkButton contrastText").addClass("lightButton contrastText");
	$('#timeSortButton').removeClass("darkButton contrastText").addClass("lightButton contrastText");
	this.sorting = sortSubmissionViews;
	this.setPosts();
}