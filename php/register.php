<?php
// register.php
// LevelledUpCurtin
// PDM Semester 1, 2016 

require "PHPMailer/PHPMailerAutoload.php";
include "postValidation.php";

// Request Fields
define("USER_FIELD", "user");
define("USERNAME_FIELD", "username");
define("PASSWORD_FIELD", "password");
define("EMAIL_FIELD", "email");

// Input limits
define("MAX_DB_LENGTH", 255);
define("MAX_USERNAME_LENGTH", 16);
define("MIN_USERNAME_LENGTH", 3);
define("MIN_PW_LENGTH", 8);

// Response messages
define("USER_EXISTS_MSG", "user already exists");
define("INVALID_CREDENTIALS_MSG", "invalid username, password or email");
define("MAIL_ERROR_MSG", "error sending mail");
define("DB_ERROR_MSG", "database error");

// Confirmation URL
define("CONFIRM_URL", "http://localhost/php/confirmemail.php");

class RegisterResponder extends POSTResponder
{
	protected function validate($fields)
	{
		parent::validate(array(USER_FIELD));

		if(!isset($_POST[USER_FIELD][USERNAME_FIELD]) ||
		   !isset($_POST[USER_FIELD][PASSWORD_FIELD]) ||
		   !isset($_POST[USER_FIELD][EMAIL_FIELD]))
		   throw new POSTException(3, INVALID_CREDENTIALS_MSG);
		
		//Clear existing sessions
		session_unset();
		$_SESSION["loggedin"] = false;
			
		$username = trim(strip_tags($_POST[USER_FIELD][USERNAME_FIELD]));
		$password = $_POST[USER_FIELD][PASSWORD_FIELD];
		$email = $_POST[USER_FIELD][EMAIL_FIELD];

		$passwordHash = password_hash($password, PASSWORD_DEFAULT);
				
		// Ensure username does not start with number
		if(is_numeric(substr($username, 0, 1)))
			throw new POSTException(3, INVALID_CREDENTIALS_MSG);
		
		// Check username length
		if(strlen($username) > MAX_USERNAME_LENGTH || 
				strlen($username) < MIN_USERNAME_LENGTH)
			throw new POSTException(3, INVALID_CREDENTIALS_MSG);
		
		// Check password length
		if(strlen($password) < MIN_PW_LENGTH)
			throw new POSTException(3, INVALID_CREDENTIALS_MSG);
			
		// Validate email address
		if(strlen($email) > MAX_DB_LENGTH ||
		   !filter_var($email, FILTER_VALIDATE_EMAIL))
			throw new POSTException(3, INVALID_CREDENTIALS_MSG);
			
		$db = new UserDatabase();

		// Does user already exist
		if(!$db->checkUser($username))
			throw new POSTException(4, USER_EXISTS_MSG);
			
		// Attempt to add
		if(!$db->insert($username, $passwordHash, $email)) 
			throw new POSTException(5, DB_ERROR_MSG);
			
		$confirmationHash = urlencode(password_hash($username, PASSWORD_DEFAULT));
		$urlUsername = urlencode($username);
		$url = CONFIRM_URL . "?user=" . $urlUsername . "&code=" . $confirmationHash; 
		
		$mailer = new PHPMailer;
		$mailer->isSMTP();
		$mailer->Host = "smtp.sendgrid.net";
		$mailer->SMTPAuth = true;
		$mailer->Username = "azure_1a611862c3fd68f9c8b5f1563a7aabc6@azure.com";
		$mailer->Password = "PDMProject2016";
		$mailer->SMTPSecure = 'tls';
		$mailer->Port = 587;
		$mailer->setFrom("levelledupcurtin@gmail.com", "LevelledUpCurtin");
		$mailer->addAddress($email);
		$mailer->WordWrap = 50;
		$mailer->isHTML(true);
		
		$mailer->Subject = "Confirm your email address for LevelledUpCurtin";
		$mailer->Body = 'Click <a href="' . $url . '">here</a> to confirm your email address';
		$mailer->AltBody = "Go here to confirm your email address " . $url;
		
		if(!$mailer->send())
			throw new POSTException(6, MAIL_ERROR_MSG);
	}
}
  
$responder = new RegisterResponder;
$responder->begin();
  
?>