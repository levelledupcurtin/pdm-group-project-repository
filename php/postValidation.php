<?php
// postValidation.php
// LevelledUpCurtin
// PDM Semester 1, 2016 

// Connect to session
session_start();

// Connect to database
include 'functions.php';

if(!isset($_SESSION["loggedin"]))
	$_SESSION["loggedin"] = false;

// Response messages
define("BAD_JSON_MSG", "invalid json");
define("GET_MSG", "POST requests only");

class POSTException extends Exception
{
	private $errorcode, $errorstring; 
	public function __construct($errorcode, $errorstring)
	{
		parent::__construct();
		$this->errorcode = $errorcode;
		$this->errorstring = $errorstring;
	}
	public function toJSONResponse()
	{
		$response = array("successful" => false, "error" => 
					array("code" => $this->errorcode, "string" => $this->errorstring));
		
		echo json_encode($response);
	}
}

abstract class POSTResponder
{
	private $responseReturned = false;
	
	public function begin()
	{
		try
		{
			if($_SERVER["REQUEST_METHOD"] !== "POST") 
				throw new POSTException(1, GET_MSG);
			$this->validate(array());
			if (!$this->responseReturned)
			{
				$this->respond();
			}
		}
		catch(POSTException $e)
		{
			$e->toJSONResponse();
		}
	}
	
	protected function validate($fields)
	{
		foreach($fields as $field)
		{
			if(!isset($_POST[$field]))
				throw new POSTException(2, BAD_JSON_MSG);
		}
	}
	
	protected function respond($jsonResponse = NULL)
	{
		$response = array("successful" => true, "error" => 
					array("code" => 0, "string" => ""), "response" => $jsonResponse);
					
		echo json_encode($response);
		$this->responseReturned = true;
	}
}

?>