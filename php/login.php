<?php
// login.php
// LevelledUpCurtin
// PDM Semester 1, 2016 

include "postValidation.php";
include "score.php";

// Request Fields
define("USER_FIELD", "user");
define("USERNAME_FIELD", "username");
define("PASSWORD_FIELD", "password");

// Response messages
define("INVALID_CREDENTIALS_MSG", "invalid username or password");

class LoginResponder extends POSTResponder
{
	protected function validate($fields)
	{
		if (isset($_POST["task"]))
		{
			switch ($_POST["task"])
			{
				case "logout":
					session_unset($_SESSION["loggedin"]);
					parent::respond("User Logged out");
					break;
				case "Check User Status":
					if ($_SESSION["loggedin"])
					{
						$scorer = new Score();
						$userInfo = new stdClass();
						$userInfo->username = $_SESSION["username"];
						$userInfo->score = $scorer->retrieveUserScore($userInfo->username);
						parent::respond($userInfo);
					}
					else
					{
						throw new POSTException(6, "User Not Logged In");
					}
					break;
				default:
					break;
			}
		}
		else
		{
			parent::validate(array(USER_FIELD));
			
			if(!isset($_POST[USER_FIELD][USERNAME_FIELD]) ||
			!isset($_POST[USER_FIELD][PASSWORD_FIELD]))
			throw new POSTException(3, INVALID_CREDENTIALS_MSG);
			
			//Clear existing sessions
			session_unset();
			$_SESSION["loggedin"] = false;
				
			$username = trim(strip_tags($_POST[USER_FIELD][USERNAME_FIELD]));
			$password = $_POST[USER_FIELD][PASSWORD_FIELD];
			
			$db = new UserDatabase();
			$databaseHash = $db->getPasswordHash($username);
			
			if(password_verify($password, $databaseHash))
			{
				//Setup session
				$_SESSION["loggedin"] = true;
				$_SESSION["username"] = $username;
			}
			else
				throw new POSTException(3, INVALID_CREDENTIALS_MSG);
			
			parent::respond();
		}
	}
}

$responder = new LoginResponder;
$responder->begin();

?>