<?php

require_once('connection.php');

class Score extends Database
{
//This method retrieves the score of the user from the database
  public function retrieveUserScore($username)
  {
    $query = "SELECT Score FROM Users WHERE Username = '$username';";
    $retScore = "";
    if(($result = $this->db->query($query)) !== NULL)
      $retScore = $result->fetch_object()->Score;

    echo $this->db->error;
    return $retScore;
  }

//Score awarded to submission owner for every vote given to them
//Based of y = mx + c
  public function onSubmissionVote($voteVal, $submission_id)
  {
    $username = ($this->db->query("SELECT Username FROM Submissions WHERE SubmissionID = '$submission_id';"))->fetch_object()->Username;
    $multiplier = $this->multiplier($username);
    $score = ($voteVal * 1.2) * $multiplier + 1;

    $this->updateScore($username, $score);

    return $this->retrieveUserScore($username);
  }

//Score awarded to suggestion owner for every vote given to them
//Based of y = mx + c
  public function onSuggestionVote($voteVal, $suggestion_id)
  {
    $username = ($this->db->query("SELECT Username FROM Suggestions WHERE SuggestionID = '$suggestion_id';"))->fetch_object()->Username;
    $multiplier = $this->multiplier($username);
    $score = ($voteVal) * $multiplier + 1;
    $this->updateScore($username, $score);

    return $this->retrieveUserScore($username);
  }

//Score given to voter for every vote given
//Score diminishes for every vote
  public function onVote($username)
  {
    $numVotes = 1;
    $score = 0;

    $query = "SELECT COUNT(*) AS dateSum FROM
    (
      SELECT Time FROM SubmissionVotes WHERE Username  = '$username'
      UNION
      SELECT Time FROM SuggestionVotes WHERE Username = '$username'
    ) AS a WHERE TIMESTAMPDIFF(HOUR, a.Time, NOW()) < 24;";

    if(($voteNumResult = $this->db->query($query)))
      $numVotes = $voteNumResult->fetch_object()->dateSum;

    $score = 2 * (1.5/($numVotes + 1));

    $this->updateScore($username, $score);

    return $this->retrieveUserScore($username);
  }

//Calculates the multplier for a particular user based on the collective vote value of
//all their submissions, suggestions, the number of number of suggestions in all of their
//submissions, and the deepest level of suggestion recurse out of all of their submissions
  private function multiplier($username)
  {
    $recurse = 0;
    $multiplierNum = 1;

    $recurseQ = "SELECT SuggestionID FROM Suggestions WHERE Username = '$username';";
    if(($recResult = $this->db->query($recurseQ)) && $recResult->num_rows > 0)
    {
      $suggIDArr = array();
      while($suggID = $recResult->fetch_array())
      {
        $suggIDArr[] = $suggID["SuggestionID"];
      }
      for($i = 0; $i < count($suggIDArr) - 1; $i++)
      {
        $deepNest = $this->levelRecurse($suggIDArr[$i], 0, 0);
        if($deepNest > $recurse)
          $recurse = $deepNest;
      }
    }

    $multiplierQ = "SELECT ((sub.sub_sum) + (sug.sug_sum))/(s_count.sug_count + 4 + 1) AS sum_result FROM
    (
      SELECT COALESCE(SUM(Value), 0) AS sub_sum FROM
      (
        SELECT SubmissionVotes.Value FROM SubmissionVotes
        INNER JOIN Submissions
        ON Submissions.SubmissionID = SubmissionVotes.SubmissionID
        WHERE Submissions.Username = '$username'
      ) AS sub_join
    ) AS sub,
    (
      SELECT COALESCE(SUM(Value), 0) AS sug_sum FROM
      (
        SELECT SuggestionVotes.Value FROM SuggestionVotes
        INNER JOIN Suggestions
        ON Suggestions.SuggestionID = SuggestionVotes.SuggestionID
        WHERE Suggestions.Username = '$username'
      ) AS sug_join
    ) AS sug,
    (
      SELECT COALESCE(COUNT(SuggestionID), 0) AS sug_count FROM
      (
        SELECT SuggestionID FROM Suggestions WHERE SubmissionID IN
        (
          SELECT SubmissionID FROM Submissions WHERE Username = '$username'
        )
      ) AS sid_count
    ) AS s_count;";

    if(($multiResult = $this->db->query($multiplierQ)))
      $multiplierNum = $multiResult->fetch_object()->sum_result;

    echo $this->db->error;
    return $multiplierNum;
  }

//Recursion function to determine the deepest nest of replies for a particular suggestion
  private function levelRecurse($suggestion_id, $rcrsLvl, $highestRcrs)
  {
    if($suggestion_id == NULL)
    {
      if($rcrsLvl > $highestRcrs)
        $highestRcrs = $rcrsLvl;
    }
    else
    {
      $query = "SELECT SuggestionID FROM Suggestions WHERE ReplySuggestionId = '$suggestion_id' ORDER BY TIME;";
      if(($result = $this->db->query($query)) !== FALSE && $result->num_rows > 0)
      {
        $resultArray = array();
        while($suggID = $result->fetch_array())
        {
          $resultArray[] = $res;
        }
        $rcrsLvl++;
        for($i = 0; $i < count($resultArray) - 1; $i++)
        {
          $this->levelRecurse($resultArray[i], $rcrsLvl, $highestRcrs);
        }
      }
    }
    return $highestRcrs;
  }

//This method simple updates the score in the database
  private function updateScore($username, $score)
  {
    $isSuccess = false;
    $query = "UPDATE Users SET Score = Score + '$score' WHERE Username = '$username';";
    if($this->processQuery($query))
      $isSuccess = true;

    echo $this->db->error;
    return $isSuccess;
  }
}

?>
