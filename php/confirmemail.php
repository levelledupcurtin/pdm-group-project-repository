<?php 
// confirmemail.php
// LevelledUpCurtin
// PDM Semester 1, 2016 
include 'functions.php';

$db = new UserDatabase();

if(isset($_GET["user"]) && isset($_GET["code"]))
{
	$username = urldecode($_GET["user"]);
	$confirmationHash = urldecode($_GET["code"]);
	if(password_verify($username, $confirmationHash))
	{
		$db->activateUser($username, true);
		// Go to homepage
		header("Location: https://levelledupcurtin.azurewebsites.net/");
	}
}

?>