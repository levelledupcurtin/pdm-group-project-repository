<?php
session_start();

require_once('connection.php');

class Categories extends Database
{
  public function addCategory($category)
  {
    $isSuccess = false;
    $query = "INSERT INTO Categories (Category) VALUES ('$category');";
    $isSuccess = $this->processQuery($query);

    return $isSuccess;
  }

  public function getAllCategories()
  {
    $catArr = array();
    $query = "SELECT * FROM Categories ORDER BY Category;";
    if(($result = $this->db->query($query)) !== FALSE)
    {
      while($category = $result->fetch_object())
      {
        $catArr[] = $category;
      }
    }
    echo $this->db->error;
    return $catArr;
  }
}

$cat = new Categories();

if(isset($_POST['get_array_categories']))
  $result = $cat->getAllCategories();
elseif(($category = json_decode(file_get_contents("php://input"))) !== NULL)
  $result = $cat->addCategory($category);

echo json_encode($result);

?>
