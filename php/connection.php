<?php
//connection.php, John Ridley and Ali Khalil , 09/04/2016
//For Project Design and Management Project
//In collaboration with LevelledUpCurtin

//connection.php establishes a connection with the database
abstract class Database
{
	protected $db;

	public function __construct()
	{
		$servername = 'au-cdbr-azure-southeast-a.cloudapp.net'; //insert servername
		$username = 'be87ca5b2df0ed'; //insert username
		$password = 'ffa441f5'; //insert password
		$dbName = 'levelledupcurtindb'; //insert database name

		$this->db = new mysqli($servername, $username, $password, $dbName);
		if($this->db->connect_errno)
		{
			die("Connection failed: " . $this->db->connect_errno);
		}
	}

	//this function sends the queries to the database
	protected function processQuery($query)
	{
		$isSuccess = FALSE;
		$result = $this->db->query($query);
		//echo $this->db->error;
		if($result !== FALSE)
		{
			$isSuccess = TRUE;
		}
		return $isSuccess;
	}

	//checks to see if the user does exist
	protected function checkUser($username)
	{
		$query = "SELECT Username FROM Users WHERE Username='$username';";
		$result = $this->db->query($query);
		return ($result->num_rows == 0);
	}

	public function __destruct()
	{
		$this->db->close();
	}
}

?>
