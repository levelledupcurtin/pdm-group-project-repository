<?php

//data_acquire.php, Ali Khalil and Cameron Minnisale, 12/04/2016
//For Project Design and Management Project
//In collaboration with LevelledUpCurtin

session_start();
$result = FALSE;

if($_SESSION["loggedin"] == true)
{
	require_once('suggestion.php');
	$subposts = new Suggestions();
	$std = new stdClass();
	$obj = json_decode(file_get_contents("php://input"));

	if($obj == NULL)
	{
		$result = json_last_error();
	}
	else
	{
		if(isset($obj->singleSuggestion))
		{
			$obj->singleSuggestion->user = $_SESSION["username"];
			$std = $subposts->insertSuggestion($obj->singleSuggestion);
		}
		elseif(isset($obj->parentSuggestion))
		{
			$obj->parentSuggestion->user = $_SESSION["username"];
			$obj->suggestion->replysuggestid = $obj->parentSuggestion->id;
			$obj->suggestion->user = $_SESSION["username"];

			$std = $subposts->insertSuggestion($obj->suggestion);
		}
		elseif(isset($obj->replysuggestid))
		{
			$obj->user = $_SESSION["username"];
			$std = $subposts->insertSuggestion($obj);
			$result = $std;
		}
		elseif(isset($obj->postId))
		{
			$obj->user = $_SESSION["username"];
			if($subposts->insertVote($obj))
				$result = TRUE;
			else
				$result = FALSE;
		}
	}
}
else
{
	$result = FALSE . ": did not log in";
}

echo json_encode($result);

?>
