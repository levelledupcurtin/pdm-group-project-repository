<?php
// userInfo.php
// LevelledUpCurtin
// PDM Semester 1, 2016 

include "postValidation.php";

class UserInfoResponder extends POSTResponder
{
	protected function validate($fields) {}
	
	protected function respond($jsonResponse = NULL)
	{
		// Get username
		if($_SESSION["loggedin"]) $username = $_SESSION["username"];
		else $username = '';
		
		// In here we will get other info (like admin status, level) to add to response
		
		$response = array("loggedin" => $_SESSION["loggedin"],
						  "username" => $username);
					
		parent::respond($response);
	}
}

$responder = new UserInfoResponder;
$responder->begin();