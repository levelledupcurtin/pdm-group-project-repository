<?php

// suggestions.php
// Cameron Minissale

require_once('connection.php');
require_once('score.php');

class Suggestions extends Database
{
	public function insertVote($std)
	{
		$scoring = new Score();
		$isSuccess = FALSE;
		$queryCheck = "SELECT * FROM SuggestionVotes WHERE Username = '$std->user' AND SuggestionID = '$std->postId';";
		if(($checkResult = $this->db->query($queryCheck)) && $checkResult->num_rows >= 1)
		{
			$query = "UPDATE SuggestionVotes SET Value = '$std->rating' WHERE Username = '$std->user' AND SuggestionID = '$std->postId';";
		}
		else
		{
			$query = "INSERT INTO SuggestionVotes (Username, SuggestionID, Value, Time) VALUES ('$std->user', '$std->postId', '$std->rating', NOW());";
			$selfVoteCheckQ = "SELECT Username FROM Suggestions WHERE SuggestionID = '$std->postId';";
			if(($sVCQRes = $this->db->query($selfVoteCheckQ)) && $sVCQRes->fetch_object()->Username != $std->user)
			{
				$scoring->onVote($std->user);
				$scoring->onSuggestionVote($std->rating, $std->postId);
			}
		}
		$isSuccess = $this->processQuery($query);
		echo $this->db->error;

		return $isSuccess;
	}

	public function getVoteAverage($suggestion_id)
	{
		$isSuccess = FALSE;
		$voteAv = -1;
		$queryGetVote = "SELECT Value FROM SuggestionVotes WHERE SuggestionID = '$suggestion_id';";

		if(($checkResult = $this->db->query($queryGetVote)) && $checkResult->num_rows >= 1)
		{
			$voteSum = 0;
			while($valObj = $checkResult->fetch_object())
			{
				$voteSum += $valObj->Value;
	 		}
			$voteAv = $voteSum/($checkResult->num_rows);
		}
	echo $this->db->error;

	return $voteAv;
  }

	public function insertSuggestion($std)
	{
		$isSuccess = false;
		$sugObject = new stdClass();

		$std->message = mysqli_real_escape_string($this->db, $std->message);

		if(isset($std->replysuggestid))
		{
			$query = "INSERT INTO Suggestions (SubmissionID, ReplySuggestionId, Username, Comment, Time)
			VALUES ('$std->parentId', '$std->replysuggestid', '$std->user', '$std->message', NOW());";
		}
		else
		{
			$query = "INSERT INTO Suggestions (SubmissionID, Username, Comment, Time)
			VALUES ('$std->parentId', '$std->user', '$std->message', NOW());";
		}

		if($isSuccess = $this->processQuery($query) !== FALSE)
		{
			$sugObject = $this->getSuggestionsFromID($this->db->insert_id);
			$sugObject->voteAv = $this->getVoteAverage($this->db->insert_id);
		}
		else
		{
			echo $this->db->error;
		}

		return $sugObject;
	}

	public function getSuggestionsFromID($suggestionID)
	{
		$output = new stdClass();
		$query = "SELECT * FROM Suggestions WHERE SuggestionID='$suggestionID' ORDER BY Time;";

		if(($result = $this->db->query($query)) !== FALSE)
		{
			if($result->num_rows == 1)
			{
				$output = $result->fetch_object();
			}
		}
		var_dump($output);
		return $output;
	}

	public function getParentSuggestionID($parent_suggestion)
	{
		$id = "";
		$query = "SELECT suggestionID FROM Suggestions WHERE Comment = '$parent_suggestion';";

		if(($result = $this->db->query($query)) !== FALSE)
		{
			$id = $result;
		}
		return $id;
	}
}
?>
