<?php

//submission_acquire.php, Ali Khalil and Cameron Minnisale, 09/04/2016
//For Project Design and Management Project
//In collaboration with LevelledUpCurtin

session_start();

require_once('submission.php');
$posts = new Submissions();
$result = FALSE;

if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true)
{
	if(isset($_POST['task']) && $_POST['task'] == 'insert_submission')
	{
		$std = new stdClass();
		$std->userName = $_SESSION["username"];
		$std->title = $_POST['title'];
		$std->submission = null;
		$std->url = null;
		if(isset($_POST['subtitle']))
			$std->submission = str_replace("\n", "<br>", $_POST['subtitle']);
		if(isset($_POST['url']))
			$std->url = $_POST['url'];
		$std->category = $_POST['category'];
		
		if($posts->insertSubmission($std))
			$result = TRUE;
		else
			$result = FALSE;
	}
	elseif(isset($_POST['task']) && $_POST['task'] == 'change_vote')
	{
		$std = new stdClass();
		
		$std->userName = $_SESSION["username"];
		$std->submission_id = $_POST["id"];
		$std->value = null;
		if(isset($_POST['vote']))
			$std->value = $_POST['vote'];

		if($posts->insertVote($std))
			$result = TRUE;
		else
			$result = FALSE;
	}
}

if(isset($_POST['task']) && $_POST['task'] == 'get_submission')
{
	$getSub = $posts->getSingleSubmission($_POST['id'], $_POST['view'] == "true");
	if(count($getSub) > 0)
		$result = $getSub;
	else
		$result = FALSE;
}
elseif(isset($_POST['task']) && $_POST['task'] == 'get_submission_category')
{
	$category = $_POST['category'];
	$getSub = $posts->getCategorySubmissions($category);
	if(count($getSub) > 0)
		$result = $getSub;
	else
		$result = FALSE;
}

echo json_encode($result);

?>
