<?php

//Submission.php, Ali Khalil, 09/04/2016
//For Project Design and Management Project
//In collaboration with LevelledUpCurtin

require_once('connection.php');
require_once('score.php');

class Submissions extends Database
{
	public function insertSubmission($std)
	{
		$isSuccess = FALSE;
		if(!$this->checkUser($std->userName))
		{
			$std->title = mysqli_real_escape_string($this->db, $std->title);
			$std->submission = mysqli_real_escape_string($this->db, $std->submission);
			$std->url = mysqli_real_escape_string($this->db, $std->url);
			$std->category = mysqli_real_escape_string($this->db, $std->category);

			$query = "INSERT INTO Submissions (Username, Title, Comment, URL, Time, Category)
								VALUES ('$std->userName', '$std->title', '$std->submission', '$std->url', NOW(), '$std->category');";
			$isSuccess = $this->processQuery($query);
		}
		echo $this->db->error;

		return $isSuccess;
	}

	public function insertVote($std)
	{
		$scoring = new Score();
		$isSuccess = FALSE;
		$queryCheck = "SELECT * FROM SubmissionVotes WHERE Username = '$std->userName' AND SubmissionID = '$std->submission_id';";
		if(($checkResult = $this->db->query($queryCheck)) && $checkResult->num_rows >= 1)
		{
			$query = "UPDATE SubmissionVotes SET Value = '$std->value' WHERE Username = '$std->userName' AND SubmissionID = '$std->submission_id';";
		}
		else
		{
			$query = "INSERT INTO SubmissionVotes (Username, SubmissionID, Value, Time) VALUES ('$std->userName', '$std->submission_id', '$std->value', NOW());";
			$selfVoteCheckQ = "SELECT Username FROM Submissions WHERE SubmissionID = '$std->submission_id';";
			if(($sVCQRes = $this->db->query($selfVoteCheckQ)) && $sVCQRes->fetch_object()->Username != $std->userName)
			{
				$scoring->onVote($std->userName);
				$scoring->onSubmissionVote($std->value, $std->submission_id);
			}
		}

		$isSuccess = $this->processQuery($query);
		echo $this->db->error;

		return $isSuccess;
	}
	
	public function getSubmissionRating($submission_id)
	{
		return $this->getVoteAverage("SELECT Value FROM SubmissionVotes WHERE SubmissionID = '$submission_id';");
	}
	
	public function getSuggestionRating($suggestion_id)
	{
		return $this->getVoteAverage("SELECT Value FROM SuggestionVotes WHERE SuggestionID = '$suggestion_id';");
	}
	
	public function getVoteAverage($query)
	{
		$isSuccess = FALSE;
		$voteAv = -1;
		if(($checkResult = $this->db->query($query)) && $checkResult->num_rows >= 1)
		{
			$voteSum = 0;
			while($valObj = $checkResult->fetch_object())
				$voteSum += $valObj->Value;
			$voteAv = $voteSum/($checkResult->num_rows);
		}
		echo $this->db->error;
		return $voteAv;
	}

	public function getAllSubmissions()
	{
		$query = "SELECT * FROM Submissions ORDER BY SubmissionID;";		
		return $this->extractSubmissions($query);
	}

	public function getSingleSubmission($submission_id, $view)
	{	
		$query = "SELECT * FROM Submissions WHERE SubmissionID = '$submission_id';";
		if($view == true)
			$this->db->query("UPDATE Submissions SET Views = Views + 1 WHERE SubmissionID = '$submission_id';");
		return $this->extractSubmissions($query, $view);
	}

	public function getCategorySubmissions($category)
	{	
		$query = "SELECT * FROM Submissions WHERE Category = '$category' ORDER BY SubmissionID;";		
		return $this->extractSubmissions($query);
	}
	
	public function extractSubmissions($query)
	{
		$scorer = new Score();
		if(($subResult = $this->db->query($query)) !== FALSE)
		{
			$subStorage = array();
			if($subResult->num_rows > 0)
			{
				while($subRow = $subResult->fetch_object())
				{
					$suggStorage = array();
					$suggestionQuery = "SELECT * FROM Suggestions WHERE SubmissionID = '$subRow->SubmissionID' ORDER BY Time;";
					if(($suggResult = $this->db->query($suggestionQuery)) !== FALSE)
					{
						while($suggRow = $suggResult->fetch_array())
						{
							$origSuggTime = strtotime($suggRow["Time"]);
							$suggRow["Time"] = new DateTime();
							$suggRow["Time"]->setTimestamp($origSuggTime + 28800);
							
							$suggRow["UserScore"] = $scorer->retrieveUserScore($suggRow["Username"]);
							$suggRow["Rating"] = $this->getSuggestionRating($suggRow["SuggestionID"]);
							$suggStorage[] = $suggRow;
						}
						$subRow->Suggestions = $suggStorage;
					}
					
					$origTime = strtotime($subRow->Time);
					$subRow->Time = new DateTime();
					$subRow->Time->setTimestamp($origTime + 28800);
          
					$subRow->VoteAverage = $this->getSubmissionRating($subRow->SubmissionID);
					$subRow->UserScore = $scorer->retrieveUserScore($subRow->Username);
					$subStorage[] = $subRow;
				}
			}
		}
		echo $this->db->error;
		return $subStorage;
	}
}
?>
