<?php
//functions.php provides functions to check, insert or remove things from mysql
//make sure to have called the functions in the order the parameters are sequenced
//otherwise mysql will chuck an error
//note: these functions assume that the incoming parameters have been validated
//note: these functions assume that the username is the primary key
include 'connection.php';

class UserDatabase extends Database
{
	//insert user to the database
	//returns true if successful, false if not
	public function insert($username, $password, $email)
	{
		$isSuccess = FALSE;
		$query = "INSERT INTO Users (Username, Password, Email)
			VALUES ('$username', '$password', '$email');";
		if($this->checkUser($username))
		{
			$isSuccess = $this->processQuery($query); 
		}
		return $isSuccess;
	}
	
	//checks to see if the user does exist
	public function checkUser($username)
	{
		$query = "SELECT Username FROM Users WHERE Username='$username';";
		$result = $this->db->query($query);
		return ($result->num_rows == 0);
	}
	
	public function activateUser($username, $isActive)
	{
		$isSuccess = FALSE;
		$sqlBool = $isActive ? "1" : "0";
		$query = "UPDATE Users SET IsVerified='$sqlBool' WHERE Username='$username'";
		if(!$this->checkUser($username))
			$isSuccess = $this->processQuery($query); 
			
		return $isSuccess;
	}
	
	public function setRootUser($username, $isRoot)
	{
		$isSuccess = FALSE;
		$sqlBool = $isRoot ? "1" : "0";
		$query = "UPDATE Users SET IsRoot='$sqlBool' WHERE Username='$username'";
		if(!$this->checkUser($username))
			$isSuccess = $this->processQuery($query); 
			
		return $isSuccess;
	}
	
	public function getPasswordHash($username)
	{
		$query = "SELECT Password FROM Users WHERE Username='$username';";
		$result = $this->db->query($query);
		$row = $result->fetch_array(MYSQLI_ASSOC);
		return $row["Password"];
	}
}

?>
