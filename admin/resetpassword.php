<?php
/**
 * resetpassword.php
 *
 * Allows an admin to reset any user password in the database
 */
session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . "/php/connection.php");

class PasswordReset extends Database
{
	public function reset($username, $newPassword)
	{
		// Check if user is logged in
		if($_SESSION["loggedin"])
		{
			$query = "SELECT * FROM Users WHERE IsAdmin = 1 AND Username = '" . $_SESSION["username"] . "';";
			$result = $this->db->query($query)->num_rows;
			// Check if user is an admin
			if($result == 1)
			{
				$newPassword = password_hash($newPassword, PASSWORD_DEFAULT);
				$query = "UPDATE Users SET Password='".$newPassword."' WHERE Username='".$username."';";
				$result = $this->db->query($query);
				echo "Password Updated (if user exists)";
			}
			else 
				echo "Insufficient Privileges";
		}
		else 
			echo "Insufficient Privileges";
	}
}

$resetter = new PasswordReset;
$resetter->reset($_POST["username"], $_POST["password"]);

?>